#include <iostream>
#include <chrono> // for clock action
#include <vector>


namespace prog_metrics {
  class ExecutionTime {
  private:
    std::vector<std::chrono::time_point<std::chrono::high_resolution_clock>> time_points;
    //std::chrono::time_point<std::chrono::high_resolution_clock> simple_start;
    //std::chrono::time_point<std::chrono::high_resolution_clock> simple_end;

  public:
    ExecutionTime () {};
    int add_time_point();
    std::chrono::microseconds duration_mys(int start_point, int end_point);
    std::chrono::milliseconds duration_ms(int start_point, int end_point);
    std::chrono::seconds duration_s(int start_point, int end_point);
    void simple_measure_start();
    void simple_measure_end();
    std::chrono::microseconds duration_simple_mys();
    std::chrono::milliseconds duration_simple_ms();
    std::chrono::seconds duration_simple_s();

  };
}
