#include "measurements.h"

namespace prog_metrics {
  int ExecutionTime::add_time_point() {
    time_points.push_back(std::chrono::high_resolution_clock::now());
    return (int) time_points.size() - 1;
  }

  std::chrono::microseconds ExecutionTime::duration_mys(int start_point, int end_point) {
    return std::chrono::duration_cast<std::chrono::microseconds>(time_points.at(end_point) - time_points.at(start_point));
  }

  std::chrono::milliseconds ExecutionTime::duration_ms(int start_point, int end_point) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(time_points.at(end_point) - time_points.at(start_point));
  }

  std::chrono::seconds ExecutionTime::duration_s(int start_point, int end_point) {
    return std::chrono::duration_cast<std::chrono::seconds>(time_points.at(end_point) - time_points.at(start_point));
  }


  void ExecutionTime::simple_measure_start() {
    time_points.push_back(std::chrono::high_resolution_clock::now());
  }
  void ExecutionTime::simple_measure_end() {
    time_points.push_back(std::chrono::high_resolution_clock::now());
  }


  std::chrono::microseconds ExecutionTime::duration_simple_mys() {
    return duration_mys(0, 1);
  }

  std::chrono::milliseconds ExecutionTime::duration_simple_ms() {
    return duration_ms(0, 1);
  }

  std::chrono::seconds ExecutionTime::duration_simple_s() {
    return duration_s(0, 1);
  }

}
