#include "tsp.h"


namespace tsp {
  tsp_solver::tsp_solver(int num_points) {
    generate_points(num_points, default_min_dist, default_max_dist);
  }

  tsp_solver::tsp_solver(std::string fileName) {
    std::ifstream file;
    point tmp;
    char delim;

    file.open(fileName);
    if(file.is_open()) {
      while (file >> tmp.x >> delim >> tmp.y) {
        points.push_back(tmp);
      }
        file.close();
    }
    else
      std::cout << "Couldn't open file to read from. Sowwy :(\n";
  }

  void tsp_solver::generate_points(int num_points) {
      generate_points(num_points, default_min_dist, default_max_dist);
  }
  void tsp_solver::generate_points(int num_points, int min_dist, int max_dist) {
    point tmp;

    if (num_points > 0) {
      std::random_device dev;
      std::mt19937 rng(dev());
      std::uniform_int_distribution<std::mt19937::result_type> distribution_cit(min_dist, max_dist);

      for (int i=0; i < num_points; ++i) {
          tmp.x = distribution_cit(rng);
          tmp.y = distribution_cit(rng);
          points.push_back(tmp);
      }
    }
    else
      std::cout << "Please provide a positive number of cities, since we cant destroy existing ones ;-)";
  }

  const void tsp_solver::print_points() {
    for (int i=0; i < (int)points.size(); ++i) {
      std::cout << "City number: " << i << "\tX-Coordinate: " << points.at(i).x << "\tY-Coordinate: " << points.at(i).y << "\n";
    }
    std::cout << "\n";
  }

  const void tsp_solver::save_points_to_file(std::string fileName) {
    std::ofstream file;
    file.open(fileName);

    if (file.is_open()) {
      for (const auto& i: points) {
          file << i.x << ", " << i.y << "\n";
      }
      file.close();
    }
    else
      std::cout << "couldn't open file to save\n";
  }

  const void tsp_solver::debug_print_vector() {
    std::cout << "{";
    for (const auto& i: points) {
      std::cout << i.x << ", " << i.y << ", ";
    }
    std::cout << "\b\b} \n";
  }
}
