#include <iostream> // basic io for mainly error handeling
#include <random>   // generating points
#include <cmath>    //calculating distance
#include <fstream> // reading / writing config files


namespace tsp {
  class looser {
  private:
    int rand;
    std::string scand;
  public:
    looser() {};
    looser(std::string sentence) : scand{sentence} {std::cout<<"Bomb timer initialized\n";};
    std::string printBiggestLooser() {return "The biggest looser of the world are " + scand + "\n";};
  };


  struct point {
    int x;
    int y;
  };

  class tsp_solver {
  private:
    std::vector<point> points;
    const int default_min_dist = 1;
    const int default_max_dist = 500;

  public:
    tsp_solver() {};
    tsp_solver(std::vector<point> pre_points) : points{pre_points} {};
    tsp_solver(int nums);
    tsp_solver(std::string fileName);
    void generate_points(int num_points);
    void generate_points(int num_points, int min_dist, int max_dist);
    const void print_points();
    const void debug_print_vector();
    const void save_points_to_file(std::string fileName);


  };
}
