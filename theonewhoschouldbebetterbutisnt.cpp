

#include <iostream>
#include <algorithm>
#include <random>
#include <cmath>
#include <chrono>
#include <fstream>

#include "tsp.h"
#include "measurements.h"


int main () {
  //time_measurement::ExecutionTime timer;
  prog_metrics::ExecutionTime time;
  time.simple_measure_start();
  auto time_start = time.add_time_point();

  tsp::looser demonstrant("Workers!");
  std::cout << demonstrant.printBiggestLooser();

  tsp::tsp_solver salesmen(12);
  salesmen.print_points();
  salesmen.debug_print_vector();
  salesmen.save_points_to_file("2nd_config.txt");

  time.simple_measure_end();

  std::cout << "Programm duration: " << time.duration_simple_mys() << "\n";
  return 0;
}
