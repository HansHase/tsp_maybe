My lovely jovely jolly readme file

[[_TOC_]]


## Project Scope

Hello fellow user who might be ended up here. Nice to know somebody is around at least.

But I must take your excitement at once. This repo might probably not be functional and is for my learning purposes. I just made it public for fun, but not for any use really. Of course you can use my code under the licence, but idk if that really helps.

So back to the learning thing. I am new to C++ (but not to programming, at least when it's without objects) and this project should help me to develop my skills and learn new stuff in C++ and the stuff that comes with it.

## Useful Links
- What is the Travelling Salesperson Problem on [Wikipedia](https://en.wikipedia.org/wiki/Travelling_salesman_problem)
- [Tsp at UWaterloo](http://www.math.uwaterloo.ca/tsp/index.html)
- [Introduction](https://nbviewer.org/url/norvig.com/ipython/TSP.ipynb) in Python
- [Real shit](https://www.math.uwaterloo.ca/~bico/papers/comp_chapter1.pdf)
- [Real shit 2'](http://akira.ruc.dk/~keld/research/LKH/LKH-1.3/DOC/LKH_REPORT.pdf)




## Compilation
```
mkdir build
cd build
cmake ..
make
```

That should probably do the trick. But it also might be broken. Sorry if so. Don't hesitate to notify me :))


## TODO (very rough)
- [ ] fix retrun type of metrics library
- [ ] Implement brute force algorithm
- [ ] Implement nearest neighbour algorithm
- [ ] Implement greedy algortim
- [ ] Write some documentation
- [ ] Improve algorithms
